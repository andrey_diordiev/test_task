$(function () {

    var Model = Backbone.Model.extend();
    var RequestModel = Backbone.Model.extend({
         url:'/index.php?d=request'
    });
    var AverageModel = Backbone.Model.extend({
        url:'/index.php?d=averageInfo'
    });
    var RequestCollection = Backbone.Collection.extend({
        model:Model,
        url:'/index.php?d=requestsList'

    });
    var ResponseCollection = Backbone.Collection.extend({
        model:Model,
        url:'/index.php?d=responseList'
    });
    var AppResponse = new ResponseCollection();
    var AppRequest = new RequestCollection();


    var AppView = Backbone.View.extend({
      el:'#content',
      initialize:function () {
          this.search = new SearchView({model:new RequestModel});
          this.request = new RequestView();
          this.response = new ResponseView();
          this.average = new AverageView({model:new AverageModel});
          this.request.on('chosen',function (id) {
              AppResponse.fetch({reset:true,data:{id:id}});
              this.average.model.fetch({data:{id:id}});
          }.bind(this));
      }
   });

   var SearchView = Backbone.View.extend({
        el:'#custom-search-input',
        events:{
          'keyup #search-input':'search'
        },
        search:function (e) {
            if(e.keyCode == 13){
                var keyword = $(e.currentTarget).val();
                this.flashMessage();
                this.model.save({keyword:keyword},{
                    isNew:true,
                    success:function (model,response) {
                        if(response == true)
                           AppRequest.fetch({reset:true});
                        else {
                            console.log(response);
                        }
                    }
                });
            }
        },
       flashMessage:function () {
           $( ".alert-info" ).animate({
               display: 'block',
               opacity: "toggle"
           }, 2000, function() {
               $( ".alert-info" ).hide();
           });
       }
   });

   var RequestView = Backbone.View.extend({
       el:'#request',
       template:_.template($('#template-request').html()),
       events:{
         'click a':'chosen'
       },
       initialize:function () {
          AppRequest.fetch({reset:true});
          AppRequest.on('reset',this.render,this);
       },
       render:function (models) {
           this.$('.content').html(this.template({
               request:models
           }))
       },
       chosen:function (e) {
           e.preventDefault();
           var id = $(e.currentTarget).data('id');
           if(id)
              this.trigger('chosen',id);
       }
   });
   var ResponseView = Backbone.View.extend({
       el:'#response',
       template:_.template($('#template-response').html()),
       initialize:function () {
          AppResponse.on('reset',this.render,this);
       },
       render:function (models) {
           this.$el.find('.content').html(this.template({
              response:models
          }));
       }
   });
   var AverageView = Backbone.View.extend({
       el:'#average',
       template:_.template($('#template-average').html()),
       initialize:function () {
           this.model.on('change',this.render,this);
       },
       render:function (models) {
           console.log('res');
           this.$el.find('.content').html(this.template({
               average:models
           }));
       }
   });
   new AppView;

});