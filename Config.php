<?php

class Config implements app\IConfig
{
    private $dbName = 'youtube';
    private $host   = 'localhost';
    private $username = 'root';
    private $password = '';

    public function getDbName()
    {
       return $this->dbName;
    }

    public function getHost()
    {
        return $this->host;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getUsername()
    {
        return $this->username;
    }
}
