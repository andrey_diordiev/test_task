<?php

require_once __DIR__ . '/autoloader/Autoloader.php';
require_once __DIR__ . '/app/App.php';
$config = require_once  __DIR__ . '/app/config.php';

(new Autoloader)->addConfig($config);
require_once __DIR__ . '/Config.php';

app\App::getInstance()->run(new Config());
