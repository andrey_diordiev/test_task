<?php

/**
 * Created by PhpStorm.
 * User: laroc
 * Date: 03.03.17
 * Time: 22:33
 */
class Autoloader
{
    protected $namespacesMap = array();
    
    public function addConfig(array $config)
    {
       foreach ($config as $alias=>$direct)
       {
          $this->addNamespace($alias,$direct);
       }

       $this->register();
    }

    protected function addNamespace($namespace, $rootDir)
    {
        if (is_dir($rootDir)) {
            $this->namespacesMap[$namespace] = $rootDir;
            return true;
        }
        return false;
    }
    protected function register()
    {
        spl_autoload_register(array($this, 'autoload'));
    }
    protected function autoload($class)
    {
        $pathParts = explode('\\', $class);
        if (is_array($pathParts)) {
            $namespace = array_shift($pathParts);
            if (!empty($this->namespacesMap[$namespace])) {
                $filePath = $this->namespacesMap[$namespace] . '/' . implode('/', $pathParts) . '.php';
                require_once $filePath;
                return true;
            }
        }
        return false;
    }
}