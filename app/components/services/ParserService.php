<?php

namespace app\components\services;

use app\components\entity\RequestEntity;
use app\components\entity\ResponseEntity;
use app\components\mapper\VideoMapper;
use app\components\parser\AbstractParser;

class ParserService
{
    /**
     * @var AbstractParser
     */
    private $parser;

    /**
     * @var VideoMapper
     */
    private $mapper;

    /**
     * ParserService constructor.
     * @param VideoMapper $mapper
     * @param AbstractParser $parser;
     * @throws \Exception
     */
    public function __construct(VideoMapper $mapper,AbstractParser $parser)
    {
        $this->parser = $parser;
        $this->mapper = $mapper;
    }

    /**
     * @param string $term
     * @return bool
     * @throws \Exception
     */
    public function request($term)
    {
        $term = mb_strtolower($term,'utf8');
        if (!is_string($term)) {
            throw new \Exception('is not string ' . $term);
        }
        $keywords = explode(',', $term);
        $this->clearSimilarKeywords($keywords);

        if (!empty($keywords)) {
           $this->handleRequests($keywords);
        }
        return true;
    }

    /**
     * @param integer $request_id
     * @return array
     */
    public function getResponse($request_id)
    {
        return $this->mapper->findResponse($request_id);
    }

    /**
     * @return array
     */
    public function getRequestsList()
    {
        return $this->mapper->findRequests();
    }

    /**
     * @param $request_id
     * @return array
     */
    public function getAverageStatistics($request_id)
    {
        return $this->mapper->averageStatistics($request_id);
    }

    /**
     * @param array $keywords
     */
    private function clearSimilarKeywords(array &$keywords)
    {
        $response = $this->mapper->findKeywords($keywords);
        if (!empty($response)) {
            foreach ($response as $item) {
                if (empty($keywords)) {
                    break;
                }
                if (($key = array_search($item['keyword'], $keywords)) !== false) {
                    unset($keywords[$key]);
                }
            }
        }
    }

    /**
     * @param array $keywords
     */
    private function handleRequests(array $keywords)
    {
        foreach ($keywords as $word) {
            $response = $this->parser->getResult($word);
            $this->saveResult(new RequestEntity(['keyword'=>$word]),$response);
        }
    }

    /**
     * @param RequestEntity $request
     * @param array $response
     */
    private function saveResult(RequestEntity $request,array $response)
    {
        $id = $this->mapper->saveRequest($request);
        foreach ($response as $item){
            if($item instanceof ResponseEntity) {
                $this->mapper->saveResponse($id, $item);
            }
        }
    }
}