<?php
namespace app\components\parser;

class ParserFactory
{
    /**
     * @param $type
     * @return AbstractParser|null
     */
   public function getParser($type)
   {
       $parser = ucfirst($type."Parser");
       $class = 'app\\components\\parser\\'.$parser;
       if(class_exists($class)){
           $parser = new $class;
           if($parser instanceof AbstractParser) {
               return $parser;
           }
       }
      return null;
   }
}