<?php

namespace app\components\parser;

use app\components\entity\ResponseEntity;
use app\components\helpers\StringHelper;
class YoutubeParser extends AbstractParser
{
    const MAX_RESPONSE = 10;
    protected $baseUrl = 'http://www.youtube.com/results?sp=EgIQAQ%253D%253D&q=';

    protected function handle($items)
    {
        $response = [];
        foreach ($items as $k=>$item){
            if(($k)==self::MAX_RESPONSE)
                break;

            $response[] = new ResponseEntity([
                'title'=>$this->getTitle($item),
                'description'=>$this->getDescription($item),
                'rating'=>$this->getViews($item)
            ]);
        }

        return $response;
    }

    /**
     * @param $item \simple_html_dom
     * @return string|null
     */
    protected function getViews($item)
    {
        if($el = $item->find('.yt-lockup-meta-info li',1)) {
           $el = StringHelper::onlyInt($el->plaintext);
        }
        return $el;
    }

    /**
     * @param \simple_html_dom $item
     * @return string|null
     */
    protected function getTitle($item)
    {
        if($el = $item->find('.yt-lockup-title a',0)) {
           $el = htmlspecialchars($el->plaintext);
        }
        return $el;
    }

    /**
     * @param \simple_html_dom $item
     * @return string|null
     */
    protected function getDescription($item)
    {
        if($el = $item->find('.yt-lockup-description',0)) {
           $el = htmlspecialchars($el->plaintext);
        }
        return  $el;
    }

    protected function getItems($html)
    {
       return $html->find('#results .item-section li .yt-lockup-content');
    }

}