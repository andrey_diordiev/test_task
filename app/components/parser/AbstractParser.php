<?php

namespace app\components\parser;

use app\components\helpers\StringHelper;

abstract class AbstractParser
{
    /**
     * @var string
     */
    protected $baseUrl;

    /**
     * @param $request string
     * @return array
     */
    public function getResult($request)
    {
        $request = StringHelper::replaceSpaces($request, '+');
        $url = $this->baseUrl . $request;
        $html = $this->getContent($url);
        $items = $this->getItems($html);

        return $this->handle($items);
    }

    /**
     * @param $url string
     * @return bool|\simple_html_dom
     */
    protected function getContent($url)
    {
        return file_get_html($url);
    }

    /**
     * @param $items\simple_html_dom
     * @return array
     */
    abstract protected function handle($items);

    /**
     * @param $html\simple_html_dom
     * @return array
     */
    abstract protected function getItems($html);
}