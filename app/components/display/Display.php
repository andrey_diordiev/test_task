<?php

namespace app\components\display;

use app\components\services\ParserService;

class Display
{
    private $service;

    public function __construct(ParserService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        require_once 'app/templates/index.html';
    }

    public function request()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        if (isset($data['keyword'])) {
            echo json_encode($this->service->request($data['keyword']));
        }
    }

    public function requestsList()
    {
        echo json_encode($this->service->getRequestsList());
    }

    public function responseList()
    {
        $result = [];

        if (isset($_GET['id'])) {
            $result = json_encode($this->service->getResponse($_GET['id']));
        }
        echo $result;
    }

    public function averageInfo()
    {
        $result = [];
        if (isset($_GET['id'])) {
            $result = json_encode($this->service->getAverageStatistics($_GET['id']));
        }
        echo $result;
    }

}