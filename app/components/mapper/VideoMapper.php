<?php

namespace app\components\mapper;

use app\components\entity\RequestEntity;
use app\components\entity\ResponseEntity;

class VideoMapper
{
    /**
     * @var \PDO
     */
    private $db;

    /**
     * VideoMapper constructor.
     * @param \PDO $db
     */
    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    /**
     * @param integer $request_id
     * @param bool $entity
     * @return array
     */
    public function findResponse($request_id, $entity = false)
    {
        $query = $this->db->prepare('SELECT * FROM response WHERE request_id = :request_id');
        $query->bindParam(':request_id', $request_id);
        $query->execute();
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        if ($entity) {
            $result = $this->iterateMap($result);
        }
        return $result;
    }

    /**
     * @param bool $entity
     * @return array
     */
    public function findRequests($entity = false)
    {
        $query = $this->db->prepare('SELECT * FROM requests');
        $query->execute();
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        if ($entity) {
            $result = $this->iterateMap($result);
        }
        return $result;
    }

    /**
     * @param array $keywords
     * @return array
     */
    public function findKeywords(array $keywords)
    {
        $items = [];
        $inQuery = implode(',', array_fill(0, count($keywords), '?'));
        $query = $this->db->prepare("SELECT * FROM requests WHERE keyword IN ({$inQuery})");
        foreach ($keywords as $k => $word) {
            $query->bindValue(($k + 1), $word);
        }

        if ($query->execute()) {
            $items = $query->fetchAll(\PDO::FETCH_ASSOC);
        }
        return $items;
    }

    /**
     * @param $request_id
     * @return array
     */
    public function averageStatistics($request_id)
    {
        $query = $this->db->prepare(
            'SELECT AVG(rating) as avg_rating, COUNT(id) as count_id,GROUP_CONCAT(" ",title) AS titles  
            FROM response WHERE request_id = :request_id');
        $query->bindParam(':request_id', $request_id);
        $query->execute();
        $result = $query->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }

    /**
     * @param integer $request_id
     * @param ResponseEntity $item
     * @return bool
     */
    public function saveResponse($request_id, ResponseEntity $item)
    {
        $query = $this->db->prepare(
            'INSERT INTO response (request_id,title,description,rating) 
         VALUES (:req_id,:title,:description,:rating)');
        $query->bindParam(':req_id', $request_id);
        $query->bindParam(':title', $item->getTitle());
        $query->bindParam(':description', $item->getDescription());
        $query->bindParam(':rating', $item->getRating());
        return $query->execute();
    }

    /**
     * @param RequestEntity $request
     * @return null|string
     */
    public function saveRequest(RequestEntity $request)
    {
        $id = null;
        $query = $this->db->prepare('INSERT INTO requests (keyword) VALUE (:keyword)');
        $query->bindParam(':keyword', $request->getKeyword());
        if ($query->execute()) {
            $id = $this->db->lastInsertId();
        }

        return $id;
    }

    /**
     * @param array $items
     * @return array
     */
    private function iterateMap(array $items)
    {
        $collection = array_map(function ($item) {
            return $this->mapRowToVideo($item);
        }, $items);

        return $collection;
    }

    /**
     * @param array $row
     * @return ResponseEntity
     */
    private function mapRowToVideo(array $row)
    {
        return new ResponseEntity($row);
    }

}