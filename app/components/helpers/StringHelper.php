<?php
/**
 * Created by PhpStorm.
 * User: laroc
 * Date: 09.03.17
 * Time: 16:48
 */

namespace app\components\helpers;


class StringHelper
{
    /**
     * @param $string
     * @return mixed
     */
    public static function onlyInt($string)
    {
       return preg_replace("/[^\d]+/","",$string);
    }

    /**
     * @param string $string
     * @param string $separator
     * @return string
     */
    public static function replaceSpaces($string,$separator)
    {
        return trim(preg_replace('/\s+/', $separator, $string));
    }
}