<?php

namespace app\components\entity;


abstract class AbstractEntity
{
    /**
     * AbstractEntity constructor.
     * @param array $state
     */

    public function __construct(array $state)
    {
        foreach ($state as $key => $item){
            if($method = $this->getMethodSetter($key))
                $this->{$method}($item);
        }
    }

    /**
     * @param string $name
     * @return bool|string
     */
    protected function getMethodSetter($name)
    {
        $methods = $this->getMethods();
        foreach ($methods as $method){
            if(strtolower($method->getName()) == 'set'.$name){
                return $method->getName();
            }
        }
        return false;
    }

    /**
     * @return \ReflectionProperty[]
     */
    protected function getMethods()
    {
        return (new \ReflectionClass($this))->getMethods(\ReflectionMethod::IS_PUBLIC);
    }
}