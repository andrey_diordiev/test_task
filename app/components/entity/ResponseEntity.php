<?php

namespace app\components\entity;

class ResponseEntity extends AbstractEntity
{
    /**
     * @var string
     */
    protected $rating;
    /**
     * @var string
     */
    protected $description;
    /**
     * @var string
     */
    protected $title;
    /**
     * @var integer
     */
    protected $request_id;
    /**
     * @var integer
     */
    protected $id;
    /**
     * @var string
     */
    protected $url;

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param string $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }
    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getRequest_id()
    {
        return $this->request_id;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }
    /**
     * @return int
     */
    public function getId()
    {
      return $this->id;
    }
}