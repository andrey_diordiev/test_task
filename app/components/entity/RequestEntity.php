<?php

namespace app\components\entity;


class RequestEntity extends AbstractEntity
{
    /**
     * @var int
     */
    protected $id;
    /**
     * @var string
     */
    protected $keyword;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * @param $id int
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param $keyword string
     */
    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;
    }
}