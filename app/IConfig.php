<?php

namespace app;

interface IConfig
{
    /**
     * @return string;
     */
    public function getHost();

    /**
     * @return string;
     */
    public function getDbName();

    /**
     * @return string
     */
    public function getUsername();

    /**
     * @return string
     */
    public function getPassword();
}