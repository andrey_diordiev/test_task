<?php

namespace app;

use app\components\display\Display;
use app\components\mapper\VideoMapper;
use app\components\parser\ParserFactory;
use app\components\services\ParserService;
use \PDO;

include_once 'library/simple_html_dom.php';

final class App
{
    /**
     * @var  IConfig;
     */
    private $config;
    /**
     * @var PDO
     */
    private $db;
    /**
     * @var App
     */
    private static $instance = null;

    protected function __construct()
    {
    }

    protected function __clone()
    {
    }

    /**
     * @return App
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @param IConfig $config
     */
    public function run(IConfig $config)
    {
        $this->config = $config;
        $this->initDb();
        $this->initDisplay();
    }

    private function initDisplay()
    {
        $action = isset($_GET['d']) ? $_GET['d'] : null;
        $type = isset($_GET['type']) ? $_GET['type'] : 'youtube';
        $parser = (new ParserFactory())->getParser($type);
        if (is_null($parser)) {
            throw new \Exception('The type of the parser is incorrect');
        }
        $display = new Display(new ParserService(new VideoMapper(self::getDb()), $parser));
        if (!is_null($action) && method_exists($display, $action)) {
            $display->{$action}();
        } else {
            $display->index();
        }
    }

    private function initDb()
    {
        $host = $this->config->getHost();
        $name = $this->config->getDbName();
        $username = $this->config->getUsername();
        $password = $this->config->getPassword();

        $this->db = new PDO("mysql:host={$host};dbname={$name};charset=utf8", $username, $password);
    }

    /**
     * @return PDO
     */
    public function getDb()
    {
        return $this->db;
    }
}